package de.sbarth.aim.ui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.prefs.Preferences;

import com.sun.org.apache.xml.internal.utils.StringComparable;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;

public class UiUtils {
    private static final String PREF_KEY_COPY_ALL_RESOLUTIONS = "pref.key.copy.all.resolutions";
    private static final String PREF_KEY_PROJECT_PATH = "pref.key.project.path";
    private static final String PREF_KEY_SELECTED_PROJECT_PATH = "pref.key.selected.project.paths";
    private static final String FILE_NAME_PROJECT_PATHS = "project-paths.txt";

    public static void setSize(final ImageView r, final double w, final double h) {
        r.maxHeight(h);
        r.minHeight(h);
        r.setFitHeight(h);
        r.maxWidth(w);
        r.minWidth(w);
        r.setFitWidth(w);
    }

    public static void setSize(final Region r, final double w, final double h) {
        r.setMaxHeight(h);
        r.setMinHeight(h);
        r.setPrefHeight(h);
        r.setMaxWidth(w);
        r.setMinWidth(w);
        r.setPrefWidth(w);
    }

    public static String getFormattedProgressValue(final Double value) {
        return new BigDecimal(value * 100).setScale(1, BigDecimal.ROUND_CEILING).toString() + " %";
    }

    public static ObservableList<String> getProjectPaths() {
        final List<String> projectPaths = FileUtils.readFile(FILE_NAME_PROJECT_PATHS);
        projectPaths.add(System.getProperty("user.home"));
        projectPaths.sort((s1, s2) -> s1.compareTo(s2));

        return FXCollections.observableArrayList(projectPaths);
    }

    public static void addProjectPath(final String projectPath) {
        final List<String> projectPaths = FileUtils.readFile(FILE_NAME_PROJECT_PATHS);
        projectPaths.add(projectPath);
        FileUtils.writeFile(FILE_NAME_PROJECT_PATHS, projectPaths);
    }

    public static String getSelectedProjectPath() {
        String selectedProjectPath = Preferences.userRoot().get(PREF_KEY_SELECTED_PROJECT_PATH, null);

        if (!getProjectPaths().contains(selectedProjectPath)) {
            selectedProjectPath = System.getProperty("user.home");
        }

        return selectedProjectPath;
    }

    public static void setSelectedProjectPath(final String projectPath) {
        Preferences.userRoot().put(PREF_KEY_SELECTED_PROJECT_PATH, projectPath);
    }

    public static void setProjectPath(final String projectPath) {
        Preferences.userRoot().put(PREF_KEY_PROJECT_PATH, projectPath);
    }

    public static String getProjectPath() {
        return Preferences.userRoot().get(PREF_KEY_PROJECT_PATH, System.getProperty("user.home"));
    }

    public static void setCopyAllResolutions(final boolean copyAllResolutions) {
        Preferences.userRoot().putBoolean(PREF_KEY_COPY_ALL_RESOLUTIONS, copyAllResolutions);
    }

    public static boolean getCopyAllResolutions() {
        return Preferences.userRoot().getBoolean(PREF_KEY_COPY_ALL_RESOLUTIONS, true);
    }

    private static class FileUtils {
        static List<String> readFile(final String fileName) {
            final List<String> values = new ArrayList<>();

            try {
                @SuppressWarnings("IOResourceOpenedButNotSafelyClosed")
                final BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    values.add(line);
                }

                bufferedReader.close();
            } catch (final Exception exc) {
                exc.printStackTrace();
            }

            return values;
        }

        static void writeFile(final String fileName, final List<String> values) {
            try {
                @SuppressWarnings("IOResourceOpenedButNotSafelyClosed")
                final BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName));

                for (final String value : values) {
                    bufferedWriter.write(value);
                    bufferedWriter.newLine();
                }

                bufferedWriter.close();
            } catch (final Exception exc) {
                exc.printStackTrace();
            }
        }
    }
}
