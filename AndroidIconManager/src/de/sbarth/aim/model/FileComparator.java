package de.sbarth.aim.model;

import java.io.File;
import java.util.Comparator;

@SuppressWarnings("ComparatorNotSerializable")
public class FileComparator implements Comparator<File> {
    @Override
    public int compare(final File file1, final File file2) {
        return file1.getPath().compareTo(file2.getPath());
    }
}